import { useEffect, useState } from 'react';

const ecuImages = [
    { name: 'next.svg', path: "AirSuspensionECU.png", is_ecu: true, functionality: ['Air suspension'] },
    { name: 'next.svg', path: "AMT.png", is_ecu: true, functionality: ['Transmission'] },
    { name: 'next.svg', path: "BCM.png", is_ecu: true, functionality: ['Wipers', 'Horn', 'Lights'] },
    { name: 'next.svg', path: "BCMA.png", is_ecu: true, functionality: ['Locking doors'] },
    { name: 'next.svg', path: "CAn_Unit.png", is_ecu: false, functionality: [] },
    { name: 'next.svg', path: "CCCM.png", is_ecu: true, functionality: ['Infotainment'] },
    { name: 'next.svg', path: "DADC_A.png", is_ecu: true, functionality: ['Driver assistance'] },
    { name: 'next.svg', path: "Data_Logger.png", is_ecu: false, functionality: [] },
    { name: 'next.svg', path: "DTS_D1D.png", is_ecu: false, functionality: [] },
    { name: 'next.svg', path: "E400.png", is_ecu: true, functionality: ['Route management'] },
    { name: 'next.svg', path: "ECM.png", is_ecu: true, functionality: ['Fuelling'] },
    { name: 'next.svg', path: "EMS.png", is_ecu: false, functionality: [] },
    { name: 'next.svg', path: "Mobile_Tracer.png", is_ecu: false, functionality: [] },
    { name: 'next.svg', path: "PCM.png", is_ecu: false, functionality: [] },
    { name: 'next.svg', path: "TCU_A.png", is_ecu: true, functionality: ['Offboard comms'] },
    { name: 'next.svg', path: "VCM.png", is_ecu: true, functionality: ['Offboard comms'] },
]

export interface EcuImage {
    name: string;
    path: string;
    is_ecu: boolean;
    functionality: string[];
}

export const genRecaptchaImages = (): EcuImage[] => {
    // let resp = await fetch("https://someurl.com", {method: "GET"});
    let captchaImages: EcuImage[] = ecuImages;

    return captchaImages;
};