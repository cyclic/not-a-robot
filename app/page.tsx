'use client';
import { Card, Image, Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, useDisclosure, Checkbox, Divider, Chip } from "@nextui-org/react";
import { useEffect, useRef, useState } from "react";
import { EcuImage, genRecaptchaImages } from "./_hooks/recaptcha.hook";

let modalOpen: boolean = false;

type ButtonProps = {
  value: string
}
function IANARButton({ value }: ButtonProps) {
  return (
    <button
      className="mt-6 transition transition-all block py-3 px-4 w-full text-white font-bold rounded cursor-pointer bg-gradient-to-r from-indigo-600 to-purple-400 hover:from-indigo-700 hover:to-purple-500 focus:bg-indigo-900 transform hover:-translate-y-1 hover:shadow-lg">
      {value}
    </button>
  )
}

type InputProps = {
  type: string,
  id: string,
  name: string,
  label: string,
  placeholder: string,
  autofocus: boolean
}
function Input({ type, id, name, label, placeholder, autofocus }: InputProps) {
  return (
    <label className="text-gray-500 block mt-3">{label}
      <input
        autoFocus={autofocus}
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        className="rounded px-4 py-3 w-full mt-1 bg-white text-gray-900 border border-gray-200 focus:border-indigo-400 focus:outline-none focus:ring focus:ring-indigo-100" />
    </label>
  )
}

type LoginFormProps = {
  onRecaptcha: (checked: boolean) => void;
}
function LoginForm({ onRecaptcha }: LoginFormProps) {
  function handleCheckbox(event: any) {
    event.preventDefault()
    onRecaptcha(event.target.checked)
  }
  return (
    <div className="bg-gray-200 flex justify-center items-center h-screen w-screen">
      <div className=" border-t-8 rounded-sm border-indigo-600 bg-white p-12 shadow-2xl w-96">
        <h1 className="font-bold text-center block text-2xl">Log In</h1>
        <form>
          <Input type="email" id="email" name="email" label="Email Address" placeholder="me@example.com" autofocus={true} />
          <Input type="password" id="password" name="password" label="Password" placeholder="••••••••••" autofocus={false} />
          <Card radius="none" shadow="none" className="p-2 mt-1 bg-zinc-100">
            <div className="grid grid-rows-1 grid-flow-col gap-4 grow-1">
              <Checkbox onChange={handleCheckbox} radius="none"><h1 className="text-center block text-l text-slate-600">I&apos;m not a robot</h1></Checkbox>
              <Image width={50} src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/RecaptchaLogo.svg/1200px-RecaptchaLogo.svg.png"></Image>
            </div>
          </Card>
          <Button isDisabled={true} className="mt-6 transition transition-all block py-3 px-4 w-full text-white font-bold rounded cursor-pointer bg-gradient-to-r from-indigo-600 to-purple-400 hover:from-indigo-700 hover:to-purple-500 focus:bg-indigo-900 transform hover:-translate-y-1 hover:shadow-lg">
            Submit
          </Button>
        </form>
      </div>
    </div>
  )
}


type IANARModalProps = {
  isOpen: boolean;
  onClose: (prevAnswerCorrect: boolean) => void;
  imageOptions: EcuImage[];
  challenge: string;
  prevAnswerCorrect: boolean;
}
function IANARModal({ isOpen, onClose, imageOptions, challenge, prevAnswerCorrect }: IANARModalProps) {
  const { onOpen, onOpenChange } = useDisclosure();
  const musicPlayers = useRef<HTMLAudioElement | undefined>(
    typeof Audio !== "undefined" ? new Audio("/rick_roll.mp3") : undefined
  );
  const [questionAnswered, setQuestionAnswered] = useState<boolean>(false);
  const [correctAnswer, setCorrectAnswer] = useState<boolean>(false);
  const [uMadBro, setUMadBro] = useState<boolean>(false);

  function onOptionSelected(option: EcuImage) {
    setQuestionAnswered(true);
    let corAns = false;
    if (option.functionality.includes(challenge)) {
      setCorrectAnswer(true)
      corAns = true;
    }else {
      setCorrectAnswer(false)
    }
    setTimeout(() => {
      console.log("timeout");
      
      onClose(corAns);
      setQuestionAnswered(false);
    }, 500);
  }

  async function rickRoll() {
    await musicPlayers.current?.play()
    setTimeout(() => {
      musicPlayers.current?.pause()
    }, 10000);
  }

  return (
    <Modal hideCloseButton={true} isOpen={isOpen} onOpenChange={onOpenChange} radius="none" className="pd-2" classNames={{
      header: 'bg-white px-1 py-2',
      body: 'bg-white',
      footer: 'bg-white'
    }}>
      <ModalContent>
        <ModalHeader className="flex flex-col gap-1 pd-15">
          <div className="bg-blue-400 p-4">
            {prevAnswerCorrect && <div>Only a robot would know that, try again</div>}
            <div>Select the ECU that is responsible for</div>
            <div className="text-4xl">{challenge}</div>
            <div>If there are none, click skip</div>
          </div>
        </ModalHeader>
        <ModalBody className="h-50%">
          <div className="grid grid-rows-4 grid-flow-col gap-1 grow-1">
            {imageOptions.map((image, idx) => (
              <Image onClick={() => onOptionSelected(image)} src={"/ECUs/" + image.path} key={idx}></Image>
            ))}
          </div>
        </ModalBody>
        <Image className={`absolute transform translate-x-16 transition-transform duration-200 ${uMadBro ? "-translate-y-10" : ""} z-1`} width={50} src="/u_mad_bro.jpg"></Image>
        <Divider className="bg-slate-300"></Divider>
        <ModalFooter className="justify-between z-10">
          <div className="flex flex-row gap-2">
            <Image onClick={() => onClose(false)} className="h-10" src="/refresh.svg"></Image>
            <Image onClick={rickRoll} className="h-10" src="/headphone.svg"></Image>
          </div>
          {questionAnswered && <Chip className="text-xl font-bold" color={correctAnswer ? 'success' : 'danger'} >{correctAnswer ? "Correct" : "Wrong!"}</Chip>}
          <Button radius="none" color="primary" onPress={() => onClose(false)}>
            Skip
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default function Home() {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [prevAnswerCorrect, setPrevAnswerCorrect] = useState<boolean>(false);

  const ecuOptions = genRecaptchaImages();
  const [recaptchaOptions, setRecaptchaOptions] = useState<EcuImage[]>(ecuOptions);
  const [challenge, setChallenge] = useState<string>("");

  useEffect(() => {
    resetRecaptcha();
  }, [])

  function shuffle(array: EcuImage[]) {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex > 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    console.log(array);
    
    return array;
  }

  function handleClose(correctAnswer: boolean) {
    setShowModal(false);
    setPrevAnswerCorrect(correctAnswer);
    resetRecaptcha()
    setTimeout(() => {
      setShowModal(true);
    }, 500)
  }

  function resetRecaptcha() {
    setRecaptchaOptions(shuffle(ecuOptions));
    const randomEcu = recaptchaOptions.find(elem => elem.functionality.length > 0);
    const challenge = randomEcu?.functionality[Math.floor(Math.random() * randomEcu.functionality.length)]
    setChallenge(challenge ? challenge : "");
  }

  return (
    <main className="flex min-h-screen flex-col items-center justify-between">
      <div className="z-10 max-w-5xl w-full items-center justify-between font-mono text-sm lg:flex">

      </div>

      <div>
        <LoginForm onRecaptcha={(checked: boolean) => setShowModal(checked)}></LoginForm>
        <IANARModal 
          prevAnswerCorrect={prevAnswerCorrect} 
          challenge={challenge} 
          isOpen={showModal} 
          onClose={(correctAnswer: boolean) => handleClose(correctAnswer)} 
          imageOptions={recaptchaOptions}
        ></IANARModal>
      </div>

    </main>
  )
}
